import java.util.LinkedList;
import java.util.List;


public class DoubleStack {

    private List<Double> data;

    public static void main(String[] argum) {
        DoubleStack mag = new DoubleStack();
        double result = mag.interpret("2 5 9 ROT + SWAP -");
        System.out.println(result);
    }

    DoubleStack() {
        this.data = new LinkedList<Double>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DoubleStack c = new DoubleStack();
        c.data.addAll(this.data);
        return c;
    }

    public boolean stEmpty() {
        if (data.size() == 0) return true;
        return false;
    }

    public void push(double a) {
        data.add(data.size(), a);
    }

    public double pop() {
        if (data.size() <= 0) {
            throw new IndexOutOfBoundsException("No data entered!");
        } else {
            return data.remove(data.size() - 1);
        }

    } // pop

    public void op(String s) {
        double double1, double2;
        if (this.data.size() < 2) {
            throw new IllegalArgumentException(String.format("At least two numbers needed for operand %s, %d provided", s, this.data.size()));
        }
        if (s.equals("-")) {
            double2 = this.pop();
            double1 = this.pop();
            this.push(double1 - double2);
        } else if (s.equals("+")) {

            double2 = this.pop();
            double1 = this.pop();
            this.push(double1 + double2);
        } else if (s.equals("*")) {
            double2 = this.pop();
            double1 = this.pop();
            this.push(double1 * double2);
        } else if (s.equals("/")) {
            double2 = this.pop();
            double1 = this.pop();
            this.push(double1 / double2);
        } else {
            throw new IllegalArgumentException(String.format("Unknown operand %s", s));
        }
    }

    public double tos() {
        if (data.size() <= 0) {
            throw new IndexOutOfBoundsException("Cannot retrieve top from empty stack!");
        } else {
            return data.get(data.size() - 1);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o.getClass() != this.getClass()) return false;
        DoubleStack oo = (DoubleStack) o;
        return this.data.equals(oo.data);
        // return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Double double1 : data) {
            sb.append(Double.toString(double1)).append(" ");
        }
        return sb.toString();
    }

    public static double interpret(String pol) {
        DoubleStack ds = new DoubleStack();
        String[] ol = pol.trim().split("\\s");
        double d;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < ol.length; i++) {
            if (ol[i].equals("")) {

            } else if (
                    ol[i].equals("-") || ol[i].equals("+") || ol[i].equals("*") || ol[i].equals("/")
            ) {
                sb.append(ol[i]).append(" ");
                try {
                    ds.op(ol[i]);
                } catch (RuntimeException e) {

                    throw new RuntimeException(String.format("Error occured:\n%s^ %s\n", sb.toString(), e.getMessage()));
                }

            } else {
                sb.append(ol[i]).append(" ");
                try {
                    d = Double.parseDouble(ol[i]);
                    ds.push(d);
                } catch (NumberFormatException e) {
                    System.out.println(ol[i]);
                    //throw new RuntimeException(String.format("Error occured:\n%s^ Cannot transform '%s' to double.\n", sb.toString(), ol[i]));
                    if (ol[i].equals("SWAP")) {
                        ds.swap();
                    } else if (ol[i].equals("ROT")) {
                        ds.rot();
                    } else {
                        //throw new NumberFormatException("Unsupported string element (not a number nor operator)");
                        // System.out.println("not a number nor operator: " + element); // DEBUG
                        throw new RuntimeException("Unsupported string element");
                    }
                }

            }


        }
        d = ds.pop();
        if (!ds.stEmpty()) {
            throw new RuntimeException("Too few operands to calculate result!");
        }
        return d;
    }

    public void swap() {
        if (data.size() >= 1){
            double d1 = pop();
            double d2 = pop();
            push(d1);
            push(d2);
        } else {
            throw new IndexOutOfBoundsException("No 2 elements in stack to SWAP");
        }
    }
    public void rot() {
        if (data.size() >= 2) {
            double d1 = pop();
            double d2 = pop();
            double d3 = pop();
            push(d2);
            push(d1);
            push(d3);
        } else {
            throw new IndexOutOfBoundsException("No 3 elements in stack to ROTATE");
        }
    }
}


/*
      With help from:
     https://github.com/herrbpl/i231-homework3/blob/master/src/DoubleStack.java
 */